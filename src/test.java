
import java.util.ArrayList;

public class test {
	public static void main(String[] args){
		Data data = new Data();
		Measurable[] list1 = new Measurable[2];
		Measurable[] list2 = new Measurable[3];
		
		Person p1 = new Person("Chat",150);
		Person p2 = new Person("Nut",165);
		
		list1[0] = p1;
		list1[1] = p2;
		
		Country c1 = new Country("CON1",20);
		Country c2 = new Country("CON2",300);
		
		BankAccount b1 = new BankAccount("BANK1", 7500);
		BankAccount b2 = new BankAccount("BANK2", 4000);
		
		list2[0] = data.min(p1, p2);
		list2[1] = data.min(c1, c2);
		list2[2] = data.min(b1, b2);
		
		System.out.println("Min of "+p1.getName() + "," + p2.getName()+" : " + data.min(p1, p2));
		System.out.println("Min of "+c1.getArea() +" ," + c2.getArea()+" : " + data.min(c1, c2));
		System.out.println("Min of "+b1.getBalance() + "," + b2.getBalance()+" : " + data.min(b1, b2));
		//System.out.println("Average of "+p1.getName()+","+p2.getName()+" : "+data.average(list1));
		//System.out.println("Average min of BankAccount, Country and Person : "+data.average(list2));		
		
		ArrayList<Taxable> Perlist = new ArrayList<Taxable>();
		ArrayList<Taxable> Comlist = new ArrayList<Taxable>();
		ArrayList<Taxable> Prolist = new ArrayList<Taxable>();
		ArrayList<Taxable> PCPlist = new ArrayList<Taxable>();
		
		TaxCalculator tax = new TaxCalculator();
		
		PersonTax per1 = new PersonTax("Chat",37000);
		PersonTax per2 = new PersonTax("Nut",25000);
		PersonTax per3 = new PersonTax("TwiTiZz",17500);
		
		Company com1 = new Company("ABC cop.",400000,90000);
		Company com2 = new Company("DEF cop.",250000,95000);
		Company com3 = new Company("GHI cop.",700000,200000);
		
		Product pro1 = new Product("computer",35000);
		Product pro2 = new Product("mobile",27000);
		Product pro3 = new Product("TV",90000);
		Product pro4 = new Product("clothing",4500);
		
		Perlist.add(per1);
		Perlist.add(per2);
		Perlist.add(per3);
		
		Comlist.add(com1);
		Comlist.add(com2);
		Comlist.add(com3);
		
		Prolist.add(pro1);
		Prolist.add(pro2);
		Prolist.add(pro3);
		
		PCPlist.add(per1);
		PCPlist.add(per2);
		PCPlist.add(per3);
		
		PCPlist.add(com1);
		PCPlist.add(com2);
		PCPlist.add(com3);
		
		PCPlist.add(pro1);
		PCPlist.add(pro2);
		PCPlist.add(pro3);
		PCPlist.add(pro4);
		
		System.out.println("-------------------------------------------------------------------------------------");	
		System.out.println("Tax of " + per1.getName() + "," + per2.getName() + ","+per3.getName() + " : " + tax.sum(Perlist));
		System.out.println("Tax of "+ com1.getComName() + "," + com2.getComName() + ","+com3.getComName() +" : " + tax.sum(Comlist));
		System.out.println("Tax of " + pro1.getProName() + "," + pro2.getProName() + ","+pro3.getProName() + " : " + tax.sum(Prolist));
		System.out.println("Sum tax of " + per1.getName() + "," + per2.getName() + ","+per3.getName() + ","+com1.getComName()+"," + com2.getComName()+","+com3.getComName()+","+pro1.getProName()+","+pro2.getProName()+","+pro3.getProName()+","+pro4.getProName()+" : "+tax.sum(PCPlist));
			
	}
	 
}
